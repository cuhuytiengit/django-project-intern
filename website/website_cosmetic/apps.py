from django.apps import AppConfig


class WebsiteCosmeticConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'website_cosmetic'
