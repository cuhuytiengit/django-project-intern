
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from backend_website.views import GetAllProductAPIView,ProductInfor
from backend_website.views import GetAllUserAPIView,UserInfor

urlpatterns = [
    path('admin/', admin.site.urls),
    path('product/', GetAllProductAPIView.as_view()),
    path('product/<int:id>/', ProductInfor.as_view()),
    path('user/', GetAllUserAPIView.as_view()),
    path('user/<int:id>/', UserInfor.as_view()),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
