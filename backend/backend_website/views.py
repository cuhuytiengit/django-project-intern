from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Product,User
from .serializers import ProductSerializer,GetAllProductSerializer
from .serializers import UserSerializer,GetAllUserSerializer
# Create your views here.
 
# API Products
class GetAllProductAPIView(APIView):
    def get(self, request):
        list_product = Product.objects.all()
        mydata = GetAllProductSerializer(list_product, many=True)
        return Response(data=mydata.data, status= status.HTTP_200_OK)


    def post(self, request):
        mydata = ProductSerializer(data=request.data)
        if not mydata.is_valid():
            return Response('Data is wrong', status=status.HTTP_400_BAD_REQUEST)
        nameproduct = mydata.data['nameproduct']
        price = mydata.data['price']
        description = mydata.data['description']
        image = mydata.data['image']
        postAPI = Product.objects.create(nameproduct= nameproduct, price=price, description= description,image=image)
        return Response(data=postAPI.id, status=status.HTTP_200_OK)
    

class ProductInfor(APIView):
    def get(self,request,id):
        try:
            obj = Product.objects.get(id=id)
        except Product.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = ProductSerializer(obj)
        return Response(mydata.data, status=status.HTTP_200_OK)
    
    def put(self,request,id):
        try:
            obj = Product.objects.get(id=id)
        except Product.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = ProductSerializer(obj, data=request.data)

        if mydata.is_valid():
            mydata.save()
            return Response(mydata.data, status=status.HTTP_205_RESET_CONTENT)
        return Response(mydata.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def patch(self,request,id):
        try:
            obj = Product.objects.get(id=id)
        except Product.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = ProductSerializer(obj, data=request.data, partial=True)

        if mydata.is_valid():
            mydata.save()
            return Response(mydata.data, status=status.HTTP_205_RESET_CONTENT)
        return Response(mydata.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self,request,id):
        try:
            obj = Product.objects.get(id=id)
        except Product.DoesNotExist:
            msg ={"msg":"Not Found"}    
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        obj.delete()
        return Response({"msg":"deleted"}, status=status.HTTP_204_NO_CONTENT)

# API User

class GetAllUserAPIView(APIView):
    def get(self, request):
        list_user = User.objects.all()
        mydata = GetAllUserSerializer(list_user, many=True)
        return Response(data=mydata.data, status= status.HTTP_200_OK)


    def post(self, request):
        mydata = UserSerializer(data=request.data)
        if not mydata.is_valid():
            return Response('Data is wrong', status=status.HTTP_400_BAD_REQUEST)
        username = mydata.data['username']
        password = mydata.data['password']
        address = mydata.data['address']
        numberphone = mydata.data['numberphone']
        postAPI = Product.objects.create(username= username, password=password, address= address, numberphone=numberphone)
        return Response(data=postAPI.id, status=status.HTTP_200_OK)
    

class UserInfor(APIView):
    def get(self,request,id):
        try:
            obj = User.objects.get(id=id)
        except User.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = UserSerializer(obj)
        return Response(mydata.data, status=status.HTTP_200_OK)
    
    def put(self,request,id):
        try:
            obj = User.objects.get(id=id)
        except User.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = UserSerializer(obj, data=request.data)

        if mydata.is_valid():
            mydata.save()
            return Response(mydata.data, status=status.HTTP_205_RESET_CONTENT)
        return Response(mydata.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def patch(self,request,id):
        try:
            obj = User.objects.get(id=id)
        except User.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = UserSerializer(obj, data=request.data, partial=True)

        if mydata.is_valid():
            mydata.save()
            return Response(mydata.data, status=status.HTTP_205_RESET_CONTENT)
        return Response(mydata.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self,request,id):
        try:
            obj = User.objects.get(id=id)
        except User.DoesNotExist:
            msg ={"msg":"Not Found"}    
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        obj.delete()
        return Response({"msg":"deleted"}, status=status.HTTP_204_NO_CONTENT)

 


