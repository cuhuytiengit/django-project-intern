from django.db import models

# Create your models here.


class Product(models.Model):
    def nameFile(instance, filename):
     return '/'.join(['images', str(instance.name), filename])
    nameproduct = models.CharField(max_length=250)
    price = models.IntegerField(default=0)
    description =  models.CharField(max_length=300)
    image = models.ImageField(upload_to=nameFile,blank=True)

class User(models.Model):
    username = models.CharField(max_length =200)
    password = models.CharField(max_length =250)
    address = models.CharField(max_length = 250)
    numberphone = models.CharField(max_length = 10)
