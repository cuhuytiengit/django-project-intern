from django.apps import AppConfig


class BackendWebsiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'backend_website'
