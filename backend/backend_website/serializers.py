from rest_framework import serializers
from .models import Product,User



class GetAllProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'nameproduct', 'price','description','image')
class ProductSerializer(serializers.Serializer):
    nameproduct =  serializers.CharField(max_length=250)
    price =  serializers.IntegerField()
    description =  serializers.CharField(max_length=300)
    image = serializers.ImageField(max_length=None, allow_empty_file=True, use_url=True)



class GetAllUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', ' username', 'password', ' address', ' numberphone')
class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length =200)
    password = serializers.CharField(max_length =250)
    address = serializers.CharField(max_length = 250)
    numberphone = serializers.CharField(max_length = 10)