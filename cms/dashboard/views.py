from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import messages
from .forms import PostProduct
from .models import  Post_Product
import json
import requests,urllib3
import collections 
import sys
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping


def index(request):
    viewproduct = Post_Product.objects.all()
    return render(request, 'dashboard.html', {'product_data':  viewproduct })
    
    
def postproduct(request): 
    if request.method == 'POST':
        fm = PostProduct(request.POST, request.FILES)
        
        if  fm.is_valid():
            nameproduct = fm.cleaned_data.get('nameproduct')
            price = fm.cleaned_data.get('price')
            description = fm.cleaned_data.get('description')
            image = fm.cleaned_data.get('image')
           
            obj =  Post_Product.objects.create(
                                nameproduct = nameproduct,
                                price  =  price,
                                description = description,
                                image =  image
                                )
           
            obj.save()
            
            print(obj)
            # print(result)
            print('nameproduct', nameproduct)
            print('img', image )
         
            # r = requests.post('http://192.168.1.6:8003/product/', data=obj)

            # if r.status_code == 200:
            #     response = r.json()
            #     print(response,'SUCCESS')
            # else:
            #     print('fail')

                
        else:
            fm = PostProduct() 
            print('post fail')
    else:
         fm = PostProduct() 


    return  render(request,'postproduct.html',{'form':fm})
    




