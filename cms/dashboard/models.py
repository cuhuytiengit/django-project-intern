from django.db import models

# Create your models here.


class Post_Product(models.Model):
    nameproduct = models.CharField(max_length=250)
    price = models.IntegerField(default=0)
    description =  models.CharField(max_length=300)
    image = models.ImageField(upload_to='image-upload/',blank=True)

class Post_User(models.Model):
    username = models.CharField(max_length =200)
    password = models.CharField(max_length =250)
    address = models.CharField(max_length = 250)
    numberphone = models.CharField(max_length = 10)
