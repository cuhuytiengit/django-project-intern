from django.core import validators
from .models import Post_Product 
from django import forms 
from django.db import models  
from django.forms import fields  

class PostProduct(forms.Form):
   nameproduct = forms.CharField(label='Name product')
   price = forms.IntegerField(label='Price')
   description =  forms.CharField(label='Description',widget=forms.Textarea)
   image = forms.ImageField(label='Image')
     