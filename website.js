var spawnRun = require("spawn-run");
const process = require("process");

try {
  // Change working directory
  process.chdir("./website");
  console.log(
    "Working directory after changing" + " directory: ",
    process.cwd()
  );
  const child1 = spawnRun("sudo docker-compose up");

  child1.stdout.on("data", (data) => {
    console.log(`stdout: ${data}`);
  });

  child1.stderr.on("data", (data) => {
    console.log(`stderr: ${data}`);
  });

  child1.on("error", (error) => console.log(`error: ${error.message}`));

  child1.on("exit", (code, signal) => {
    if (code) console.log(`Process exit with code: ${code}`);
    if (signal) console.log(`Process killed with signal: ${signal}`);
    console.log(`Done ✅`);
  });
} catch (err) {
  // printing error if occurs
  console.error("error occurred while changing" + " directory: ", err);
}
