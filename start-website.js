const { fork } = require("child_process");

const website = fork("website.js");
const backend = fork("backend.js");
